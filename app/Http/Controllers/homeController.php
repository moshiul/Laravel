<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class homeController extends Controller
{
    public function contract()
    {
        return view('users/contract');
    }

    public function about($id,$name)
    {
        return view('about',compact('id','name'));
    }


}
