<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/contract', 'homeController@contract');

Route::get('/about/{id}/{name}','homeController@about');

/*Route::get('/show/{id}/{name}',function($id,$name){
    return view('users/show',compact('id','name'));
});*/


